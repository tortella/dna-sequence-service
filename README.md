# DNA Sequence Service

Service responsible for analyze Human and Simian DNA

## Dependencies
```
node 12.x
```

### Framework
```
NestJS: https://nestjs.com/
```

## Build

Installing NodeJs
```sh
We recommend to use NVM (Node Version Menager)
https://github.com/nvm-sh/nvm
```

Installing dependencies:

```sh
npm install
```

Setting up local database using docker:
```sh
docker build -t "simian" src/resources/db/ && docker run --rm -d -p 5432:5432 -e POSTGRES_DB=dna_sequence_service -e POSTGRES_USER=root -e POSTGRES_PASS=root simian
```

Running NestJs project:

```sh
npm run start
```

## Tests
```sh
npm run test
```

## Swagger
https://dna-sequence-service.herokuapp.com/

## Monitoring
TBD

## Roadmap
```sh
Add monitoring to the application. E.g: New Relic / Grafana / Prometheos
Add validation to /simian API to handle only valid bodies (NxN) and containing only A, T, C, G
Add integration tests with DB container
Improve test coverage
Improve isSiman logic to perform better than On²
```
