import {Module} from '@nestjs/common';
import {DnaSequenceModule} from './dna-sequence/dna-sequence.module';
import {TypeOrmModule} from "@nestjs/typeorm";

@Module({
  imports: [
      DnaSequenceModule,
      TypeOrmModule.forRoot({
          type: 'postgres',
          host: process.env.DATABASE_HOST || 'localhost',
          port: 5432,
          username: process.env.DATABASE_USER || 'root',
          password: process.env.DATABASE_PASSWORD || 'root',
          database: process.env.DATABASE || 'dna_sequence_service',
          entities: ['dist/**/*.entity.js'],
          synchronize: false,
        }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
