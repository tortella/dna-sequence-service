import {Body, Controller, ForbiddenException, Get, HttpCode, Post, ValidationPipe} from '@nestjs/common';
import {DnaSequenceService} from "./dna-sequence.service";
import {DtoDna} from "../model/dna-dto";
import {ApiForbiddenResponse, ApiOkResponse} from "@nestjs/swagger";

@Controller('dna')
export class DnaSequenceController {

    constructor( private dnaSequenceService: DnaSequenceService) {

    }

    @ApiOkResponse({description: "DNA sent is Mutant dna"})
    @ApiForbiddenResponse({description: "DNA sent is Human dna"})
    @HttpCode(200)
    @Post("/simian")
    async isSimian(@Body(ValidationPipe) dtoDna: DtoDna) {
        if (await this.dnaSequenceService.isSimian(dtoDna.dna)) {
            return {
                "isSimian": "true"
            }
        } else {
            throw new ForbiddenException("DNA sent is Human dna")
        }
    }

    @Get("/stats")
    async getStats() {
        return await this.dnaSequenceService.getStats();
    }
}
