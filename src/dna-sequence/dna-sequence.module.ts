import { Module } from '@nestjs/common';
import { DnaSequenceController } from './dna-sequence.controller';
import { DnaSequenceService } from './dna-sequence.service';
import {DnaSequenceRepository} from "./dna-sequence.repository";
import {TypeOrmModule} from "@nestjs/typeorm";

@Module({
  imports: [TypeOrmModule.forFeature([DnaSequenceRepository])],
  controllers: [DnaSequenceController],
  providers: [DnaSequenceService]
})
export class DnaSequenceModule {}
