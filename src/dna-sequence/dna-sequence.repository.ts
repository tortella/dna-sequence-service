import {EntityRepository, Repository} from "typeorm";
import {Dna} from "./dna.entity";
import {DnaTypeEnum} from "../model/dna-type-enum";

@EntityRepository(Dna)
export class DnaSequenceRepository extends Repository<Dna> {

    constructor() {
        super();
    }

    async getDnaSequenceByHash(dnaHash: string | Int32Array): Promise<Dna> {
        const query = this.createQueryBuilder('dna');

        query.andWhere('dna.dna_hash = :dnaHash', {dnaHash});

        return await query.getOne();
    }

    async storeDnaSequence(dna: String[], dnaHash: string | Int32Array, type: DnaTypeEnum) {
        const values = {
            dna: JSON.stringify(dna),
            dna_hash: dnaHash,
            dna_type: type,
            created_at: `now()`
        };

        await this.createQueryBuilder()
            .insert()
            .into('dna')
            .values(values)
            .execute()
            .catch(err => {
                console.log(err, "Failed to insert dna sequence:");
                throw err
            });
    }

    async getStats() {
        const query = this.createQueryBuilder('dna');

        query.select('count(dna.dna_type), dna_type');
        query.groupBy('dna.dna_type');

        return await query.getRawMany();
    }
}
