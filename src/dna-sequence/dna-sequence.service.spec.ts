import {Test, TestingModule} from '@nestjs/testing';
import {DnaSequenceService} from './dna-sequence.service';

import {DnaSequenceRepository} from "./dna-sequence.repository";

const mockRepository = () => ({
    getDnaSequenceByHash: jest.fn(),
    storeDnaSequence: jest.fn(),
    getStats: jest.fn(),
});

describe('DnaSequenceService', () => {
    let service;
    let repository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [DnaSequenceService, {provide: DnaSequenceRepository, useFactory: mockRepository}],
        }).compile();

        service = await module.get<DnaSequenceService>(DnaSequenceService);
        repository = await module.get<DnaSequenceRepository>(DnaSequenceRepository)
    });


    describe('isSimian', () => {
        it('isSimian should return false for Human DNA', async () => {

            let isSimian = await service.isSimian(
                [
                    "ATGCGA",
                    "CCGTTC",
                    "TTATGT",
                    "AGAAGG",
                    "CACCTA",
                    "TCACTG"
                ]);

            expect(isSimian).toBe(false)
        });

        it('isSimian should return true for Simian DNA (Horizontal)', async () => {

            let isSimian = await service.isSimian(
                [
                    "AAAAGA",
                    "CCGTTC",
                    "TTATGT",
                    "AGAAGG",
                    "CACCTA",
                    "TCACTG"
                ]);

            expect(isSimian).toBe(true)
        });

        it('isSimian should return true for Simian DNA (Vertical)', async () => {

            let isSimian = await service.isSimian(
                [
                    "ATCGGA",
                    "CCGGTC",
                    "TTAGGT",
                    "AGAGGG",
                    "CACCTA",
                    "TCACTG"
                ]);

            expect(isSimian).toBe(true)
        });

        it('isSimian should return true for Simian DNA (Diagonal left to right)', async () => {

            let isSimian = await service.isSimian(
                [
                    "AAAAGA",
                    "CCGTTC",
                    "ATATGT",
                    "AAAAGG",
                    "CAACTA",
                    "TCAATG"
                ]);

            expect(isSimian).toBe(true)
        });

        it('isSimian should return true for Simian DNA (Diagonal right to left)', async () => {

            let isSimian = await service.isSimian(
                [
                    "AAAAGA",
                    "CCGTTC",
                    "TTATCT",
                    "AGACGG",
                    "CACCTA",
                    "TCACTG"
                ]);

            expect(isSimian).toBe(true)
        });
    });


    describe('getStats', () => {
        it('getStats should return 2 ratio', async () => {

            repository.getStats.mockResolvedValue([
                {
                    "dna_type": "SIMIAN",
                    "count": 2
                },
                {
                    "dna_type": "HUMAN",
                    "count": 1
                },
            ]);

            const result = await service.getStats();

            expect(result.count_mutant_dna).toBe(2);
            expect(result.count_human_dna).toBe(1);
            expect(result.ratio).toBe(2);
        });

        it('getStats should return 0.5 ratio', async () => {

            repository.getStats.mockResolvedValue([
                {
                    "dna_type": "SIMIAN",
                    "count": 1
                },
                {
                    "dna_type": "HUMAN",
                    "count": 2
                },
            ]);

            const result = await service.getStats();

            expect(result.count_mutant_dna).toBe(1);
            expect(result.count_human_dna).toBe(2);
            expect(result.ratio).toBe(0.5);
        })
    })
});


