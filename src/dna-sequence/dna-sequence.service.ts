import {Injectable} from '@nestjs/common';
import {Md5} from "ts-md5";
import {DnaSequenceRepository} from "./dna-sequence.repository";
import {InjectRepository} from "@nestjs/typeorm";
import {DnaTypeEnum} from "../model/dna-type-enum";

const _ = require('lodash');

@Injectable()
export class DnaSequenceService {

    constructor(@InjectRepository(DnaSequenceRepository)
                private repository: DnaSequenceRepository) {
    }

    async isSimian(dna: string[]) {

        const dnaHash = Md5.hashStr(JSON.stringify(dna));
        const dbHash = await this.repository.getDnaSequenceByHash(dnaHash);

        if (dbHash) {
            return dbHash.dna_type === DnaTypeEnum.SIMIAN
        } else {
            let isSimian = false;
            for (let x = 0; x < dna.length; x++) {
                for (let y = 0; y < dna.length; y++) {

                    if (y < dna.length - 3) {
                        if (this.checkHorizontal(dna, x, y)) {
                            isSimian = true;
                            break;
                        }
                    }
                    if (x < dna.length - 3) {
                        if (this.checkVertical(dna, x, y)) {
                            isSimian = true;
                            break;
                        }
                    }
                    if (x < dna.length - 3 && y < dna.length - 3) {
                        if (this.checkDiagonalLeftToRight(dna, x, y)) {
                            isSimian = true;
                            break;
                        }
                    }
                    if (x > dna.length - 3 && y > dna.length - 3) {
                        if (this.checkDiagionalRightToLeft(dna, x, y)) {
                            isSimian = true;
                            break;
                        }
                    }
                }
            }

            await this.repository.storeDnaSequence(dna, dnaHash, isSimian ? DnaTypeEnum.SIMIAN : DnaTypeEnum.HUMAN);
            return isSimian;
        }

    }

    private checkHorizontal(dna: string[], x: number, y: number) {
        let char = dna[x].charAt(y);
        return char == dna[x].charAt(y + 1) && char == dna[x].charAt(y + 2) && char == dna[x].charAt(y + 3)
    }

    private checkVertical(dna: string[], x: number, y: number) {
        let char = dna[x].charAt(y);
        return char == dna[x + 1].charAt(y) && char == dna[x + 2].charAt(y) && char == dna[x + 3].charAt(y)
    }

    private checkDiagonalLeftToRight(dna: string[], x: number, y: number) {
        let char = dna[x].charAt(y);
        return char == dna[x+1].charAt(y + 1) && char == dna[x+2].charAt(y + 2) && char == dna[x+3].charAt(y + 3)
    }

    private checkDiagionalRightToLeft(dna: string[], x: number, y: number) {
        let char = dna[x].charAt(y);
        return char == dna[x -1].charAt(y - 1) && char == dna[x - 2].charAt(y -2) && char == dna[x -3 ].charAt(y - 3)
    }

    async getStats() {
        let stats = await this.repository.getStats();
        const mutantStats = _.find(stats, (it) => it.dna_type === "SIMIAN");
        const humanStats = _.find(stats, (it) => it.dna_type === "HUMAN");
        const countMutantDna = !!mutantStats ? +mutantStats.count : 0;
        const countHumanDna = !!humanStats ? +humanStats.count : 0;

        return {
            "count_mutant_dna": countMutantDna,
            "count_human_dna": countHumanDna,
            "ratio": +(countMutantDna / (countHumanDna > 0 ? countHumanDna : 1)).toFixed(2)
        }
    }
}
