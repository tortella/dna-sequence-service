import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class Dna extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    dna_id: string;

    @Column({ type: 'text' })
    dna_hash: string;

    @Column({ type: 'text' })
    dna: string;

    @Column({ type: 'text' })
    dna_type: string;

    @Column({ type: 'timestamptz' })
    created_at: Date;
}
