export enum DnaTypeEnum {
    HUMAN = 'HUMAN',
    SIMIAN = 'SIMIAN'
}
