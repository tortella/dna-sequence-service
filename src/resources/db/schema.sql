CREATE SCHEMA "dna_sequence_service";
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

ALTER DATABASE "dna_sequence_service" SET search_path TO dna_sequence_service, public;

SET SCHEMA 'dna_sequence_service';

create table dna
(
  dna_id   uuid default public.uuid_generate_v4() not null,
  dna_hash            text not null,
  dna                 text not null,
  dna_type                text not null,
  created_at          timestamptz not null
);

create unique index on dna (dna_hash);
create index on dna (dna_type);
